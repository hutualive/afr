/*
 * Amazon FreeRTOS V201906.00 Major
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

#ifndef AWS_CLIENT_CREDENTIAL_KEYS_H
#define AWS_CLIENT_CREDENTIAL_KEYS_H

/*
 * @brief PEM-encoded client certificate.
 *
 * @todo If you are running one of the Amazon FreeRTOS demo projects, set this
 * to the certificate that will be used for TLS client authentication.
 *
 * @note Must include the PEM header and footer:
 * "-----BEGIN CERTIFICATE-----\n"\
 * "...base64 data...\n"\
 * "-----END CERTIFICATE-----\n"
 */
#define keyCLIENT_CERTIFICATE_PEM                   \
"-----BEGIN CERTIFICATE-----\n"\
"MIIDWTCCAkGgAwIBAgIUUFvCyPaIOohzArYswQGv75eiP+0wDQYJKoZIhvcNAQEL\n"\
"BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g\n"\
"SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIwMDExMDAzMDk1\n"\
"MloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0\n"\
"ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMiCSjhdcjcWKod2MRPD\n"\
"GOb+RVZxT1/9AGv1d1sYtLPeu9QMoV2S2EcWrMz0Cr204/SwDsyzwcROnizxa8hH\n"\
"kkU7kIGIbD2OqxwhDDgTY0I68HszOEZ5lSXeXTUiSgs3HZCG4NPM1JQgDvKmMZd6\n"\
"919lRXBJpkDLErv0gP01cxUKY4MYqqOeC7+y1k/JB2WTHriuIDL3k9ZeLwykXT9q\n"\
"sjDi3IaAmeWe9bwR1Nte/nrUOGcr8+zC0aHP76/7nr9ZLUm2D+GxNMqucoHVlAyp\n"\
"mHWHWVGfpwEdmsUDWPNigYjH8uYy4ZxsP2XMJnEuz9AAFeHgUvnX1GykQ9L9P50n\n"\
"nEMCAwEAAaNgMF4wHwYDVR0jBBgwFoAUHqUe2NXkIBAom+aJJR5ofAac6PIwHQYD\n"\
"VR0OBBYEFEPxQhVOc2Cw4NprlPAxcyY0R8/AMAwGA1UdEwEB/wQCMAAwDgYDVR0P\n"\
"AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBCSs4j9mAxLYGsEESykErEFJLv\n"\
"+K7g3N4BN3bqVzGEgzKsRp0dZZj4zz6ddIEe2vhiHoOOYbukieZ/0//LkvVAs1YR\n"\
"LapBJeIpjy9AGL77Mww3BfJhtpC6ex4Ao/LTf5YQ03TpnDur4nGrHl+rFVh9q90C\n"\
"kSa/uGcUo4QaGKaceXkFOzuv8X0jn6oWm90R163lCTQAqpMmrKXVSqclhx0UU3n0\n"\
"+adnpMw7YdSBIsriix8ivcD37/3U1Hjs4XmUoritF1F052z6l/JsGdA5UuKnAIvV\n"\
"bF8K8VaoXphPC04XzisUiy47XevIuMolp07QCN2keY0BVLKjTvj3qNvNzTQn\n"\
"-----END CERTIFICATE-----"

/*
 * @brief PEM-encoded issuer certificate for AWS IoT Just In Time Registration (JITR).
 *
 * @todo If you are using AWS IoT Just in Time Registration (JITR), set this to
 * the issuer (Certificate Authority) certificate of the client certificate above.
 *
 * @note This setting is required by JITR because the issuer is used by the AWS
 * IoT gateway for routing the device's initial request. (The device client
 * certificate must always be sent as well.) For more information about JITR, see:
 *  https://docs.aws.amazon.com/iot/latest/developerguide/jit-provisioning.html,
 *  https://aws.amazon.com/blogs/iot/just-in-time-registration-of-device-certificates-on-aws-iot/.
 *
 * If you're not using JITR, set below to NULL.
 *
 * Must include the PEM header and footer:
 * "-----BEGIN CERTIFICATE-----\n"\
 * "...base64 data...\n"\
 * "-----END CERTIFICATE-----\n"
 */
#define keyJITR_DEVICE_CERTIFICATE_AUTHORITY_PEM    NULL

/*
 * @brief PEM-encoded client private key.
 *
 * @todo If you are running one of the Amazon FreeRTOS demo projects, set this
 * to the private key that will be used for TLS client authentication.
 *
 * @note Must include the PEM header and footer:
 * "-----BEGIN RSA PRIVATE KEY-----\n"\
 * "...base64 data...\n"\
 * "-----END RSA PRIVATE KEY-----\n"
 */
#define keyCLIENT_PRIVATE_KEY_PEM                   \
"-----BEGIN RSA PRIVATE KEY-----\n"\
"MIIEpAIBAAKCAQEAyIJKOF1yNxYqh3YxE8MY5v5FVnFPX/0Aa/V3Wxi0s9671Ayh\n"\
"XZLYRxaszPQKvbTj9LAOzLPBxE6eLPFryEeSRTuQgYhsPY6rHCEMOBNjQjrwezM4\n"\
"RnmVJd5dNSJKCzcdkIbg08zUlCAO8qYxl3r3X2VFcEmmQMsSu/SA/TVzFQpjgxiq\n"\
"o54Lv7LWT8kHZZMeuK4gMveT1l4vDKRdP2qyMOLchoCZ5Z71vBHU217+etQ4Zyvz\n"\
"7MLRoc/vr/uev1ktSbYP4bE0yq5ygdWUDKmYdYdZUZ+nAR2axQNY82KBiMfy5jLh\n"\
"nGw/ZcwmcS7P0AAV4eBS+dfUbKRD0v0/nSecQwIDAQABAoIBAHxhRkU8wnMljvBF\n"\
"AyCtL69rH/wFgiWbPj26IXYjF0VU79MUmqz/QKaojDz/TgJsSdQpuqSFBbgOxNRA\n"\
"VooAy92im3ue8vYkoERsw7WtYS9efHsQROKC3WCGVe91+9SFNhPHd4QrVdslH+Cp\n"\
"861yHC3TFoYXSch7sH1jqPhpxZ/QneabirKx1CDshBLMHViqzw59puEudqf7r3UO\n"\
"95A1lQu4+qhgrfnqn3P39N1Wm41GGPEMPqzkbMo4wt/yfw9DEQxVq7GLamsfLvKS\n"\
"iBV0BIVIb2lMzJivfztKvTwRYTtaCafB89nJuzLVh8KKhtVfLyPX51MVTSybd8+i\n"\
"Ne9xoOECgYEA9jvi2ACoEx4oZRLUqCQiuEmVjBBTFHjVL1uJi0SD33RTGFDi8Vmz\n"\
"UrPurCbuFjK3/kl3oJq1qUHOqTEH07mxnJSmTw1eYl9xO5p0S/05qiig8mL00zfN\n"\
"hLc1RH+vBExGmvs1sxsR/4ZNSf631gPg6InGTZVbxBWSukxymzZBh8sCgYEA0HYj\n"\
"qp+oaWGSRQMSKzZBKVT1dSx+yraF11koV6V0xpzjJkVjSFYqRW3GfIkYdBbh1ue7\n"\
"NB4oJhqgghL2RItAd9Z03vjTqOFWHM93lNvwepcfoA+Mk7LE0j4NmeTRPagjj7+x\n"\
"Z89c6cn0f627EDcoqEGGxrfXR/s6dyGZjmozfmkCgYEAzhLQhnYGM/nJVM2trAqS\n"\
"BCmRPudqoHd/82JoemWc+CYiZPdM9zBbI5vy46CxIWLRGb/inkbcQgsRKggexNNk\n"\
"EedWSFKn6yx1V6ysJ+yFliLqj/DBdQP+vFOYAPvbPX2YmZwkFQFSzsdXZ9YFhNc6\n"\
"I2Mq00NsyW5EniXcxIljzoECgYAIzsUJA3/pG+9DLTMKUluEdIt+bOfahsW26JR0\n"\
"x4/ciGpshogAWWulzVKSlnRLC2B1ZQsa059cpMldVZHEKaIKriv388l1nDe4NefW\n"\
"iUhq1ZHHgICvCOGaBHcspmdU1JLUEwRa338MfbTv5BZARCRan5IjOUdGSk4ZFHlZ\n"\
"dBFC6QKBgQCXl9dbSJ/uGVENm4ke6UyA5YFcU1sKHd9sKaCIwknwKkchKuY3+Btl\n"\
"R31QCVittDW+wzeaD610f5xoDtQNcNPH4FUE6bHzvVwO5EI7jbn+H/vodVy9SkhV\n"\
"eYzpzv1UnQ63isRr3Ml/oo72v7AYLLAUC6HlntfR/IF4VsdHMamcQw==\n"\
"-----END RSA PRIVATE KEY-----"

#endif /* AWS_CLIENT_CREDENTIAL_KEYS_H */
