/**
  ******************************************************************************
  * @file    aws_flash.c
  * @brief   This file provides code for writing and erasing the FLASH.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "string.h"
#include "stm32h7xx_hal.h"
#include "FreeRTOS.h"

#include "aws_flash.h"

BaseType_t AWS_FlashProgramBlock( const uint8_t * pucAddress,
                                  const uint8_t * pucData,
                                  int32_t lSize )
{
    uint32_t ulAddress 	= (uint32_t) pucAddress;
    uint32_t ullData 	= (uint32_t) pucData;
    uint8_t xWriteBuffer[ FLASHWORD_SIZE ];

    HAL_FLASH_Unlock();

    /*
     *  If the address is not aligned to a 256-bit boundary, the write must be
     *  offset by preserving the preceding bits.
     */
    if( ulAddress % FLASHWORD_SIZE != 0 )
    {
        uint32_t ulBaseAddress = ( ulAddress / FLASHWORD_SIZE ) * FLASHWORD_SIZE;
        uint32_t ulOffset = ulAddress - ulBaseAddress;
        uint32_t ulWriteLength = ( lSize <= ( FLASHWORD_SIZE - ulOffset ) )? lSize : ( FLASHWORD_SIZE - ulOffset );

        /*
         *  Fill the write buffer with 0xFF (empty) bytes
         */
        memset( xWriteBuffer, 0xFF, FLASHWORD_SIZE );

        /*
         *  Copy the original bytes from FLASH
         */
        memcpy( xWriteBuffer, ( uint8_t * ) ulBaseAddress, FLASHWORD_SIZE - ulOffset );

        /*
         *  Copy the new bytes from the buffer
         */
        memcpy( xWriteBuffer + ulOffset, ( uint8_t * ) ullData, ulWriteLength );

        if ( HAL_OK == HAL_FLASH_Program( FLASH_TYPEPROGRAM_FLASHWORD, ulBaseAddress, (uint64_t) (uint32_t) xWriteBuffer ) )
        {
            ulAddress = ulBaseAddress + FLASHWORD_SIZE;
            ullData += ulWriteLength;
            lSize -= ulWriteLength;
        }
        else
        {
            HAL_FLASH_Lock();

            return pdFAIL;
        }
    }

    while( lSize > 0 )
    {
        if( lSize < FLASHWORD_SIZE )
        {
            memset( xWriteBuffer, 0xFF, FLASHWORD_SIZE );
            memcpy( xWriteBuffer, ( uint8_t * ) ullData, lSize );
            ullData = ( uint64_t ) ( uint32_t ) xWriteBuffer;
        }
        if ( HAL_OK == HAL_FLASH_Program( FLASH_TYPEPROGRAM_FLASHWORD, ulAddress, ullData ) )
        {
            ulAddress += FLASHWORD_SIZE;
            ullData += FLASHWORD_SIZE;
            lSize -= FLASHWORD_SIZE;
        }
        else
        {
            HAL_FLASH_Lock();

            return pdFAIL;
        }
    }

    HAL_FLASH_Lock();

    return pdPASS;
}

BaseType_t AWS_FlashEraseSector( BaseType_t cBank, BaseType_t cSector )
{
    BaseType_t xReturn = pdFAIL;

    uint32_t SectorError = 0xFFFFFFFFUL;

    /* Program this new file in the upper flash bank. */
    FLASH_EraseInitTypeDef xEraseInit =
    {
        .TypeErase      = FLASH_TYPEERASE_SECTORS,
        .Banks          = cBank,
        .Sector         = cSector,
        .NbSectors      = 1,
        .VoltageRange   = FLASH_VOLTAGE_RANGE_1
    };

    HAL_FLASH_Unlock();

    if( FLASH_BANK_1 == cBank )
    {
        __HAL_FLASH_CLEAR_FLAG_BANK1( FLASH_FLAG_ALL_BANK1 );
    }
    else
    {
        __HAL_FLASH_CLEAR_FLAG_BANK2( FLASH_FLAG_ALL_BANK2 );
    }

    if( HAL_OK == HAL_FLASHEx_Erase( &xEraseInit, &SectorError ) )
    {
        xReturn = pdPASS;
    }

    return xReturn;
}
