/**
  ******************************************************************************
  * @file    aws_flash.h
  * @brief   This file provides code for writing and erasing the FLASH.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AWS_FLASH_H_
#define __AWS_FLASH_H_

#ifdef __cplusplus
extern "C" {
#endif

#define FLASHWORD_SIZE			( 256 / 8 )

BaseType_t AWS_FlashProgramBlock( const uint8_t* pucAddress,
                                  const uint8_t* pucData,
                                  int32_t lSize );

BaseType_t AWS_FlashEraseSector( BaseType_t cBank, BaseType_t cSector );

#ifdef __cplusplus
}
#endif

#endif /* __AWS_FLASH_H_ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
