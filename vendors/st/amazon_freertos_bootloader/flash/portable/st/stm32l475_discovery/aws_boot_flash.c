/**
  ******************************************************************************
  * File Name          : aws_boot_flash.c
  * Description        : This file provides code that interacts with the FLASH
  *                      memory on the STM32L475VG.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */


/**
 * @file aws_boot_flash.c
 * @brief Boot flash implementation.
 */

/* ST includes.*/
#include "stm32l4xx_hal.h"

/* Bootloader includes.*/
#include "aws_boot_types.h"
#include "aws_boot_flash.h"
#include "aws_boot_loader.h"
#include "aws_boot_partition.h"
#include "aws_boot_flash_info.h"
#include "aws_boot_log.h"
#include "aws_boot_config.h"

/*-----------------------------------------------------------*/

BaseType_t AWS_FlashErasePartition( uint8_t ucFlashArea );
BaseType_t AWS_FlashSwapSetFlag( BaseType_t Flag );

BaseType_t BOOT_FLASH_Write( const uint32_t * pulAddress,
                             const uint32_t * pulData,
                             int lLength )
{
    BaseType_t xReturn = pdFALSE;
    
    /* Flash write must occur on a write boundary (64 bit) and a full 64 bits
       must be written. */
    if ( ( ( (uint32_t) pulAddress % FLASHWORD_SIZE) == 0 ) &&
         ( ( lLength  % FLASHWORD_SIZE ) == 0 ) )
    {
        uint32_t ulBlocks = lLength / FLASHWORD_SIZE;
        uint32_t ulAddress = (uint32_t) pulAddress;
        uint64_t ulData = (uint64_t) pulData;
        
        HAL_FLASH_Unlock();
        
        xReturn = pdTRUE;
        
        while ( ulBlocks-- )
        {
            if ( HAL_OK != HAL_FLASH_Program( FLASH_TYPEPROGRAM_DOUBLEWORD, ulAddress, * (uint64_t *) ulData ) )
            {
                xReturn = pdFALSE;
                break;
            }
            else
            {
                ulAddress += FLASHWORD_SIZE;
                ulData += FLASHWORD_SIZE;
            }
        }
        
        HAL_FLASH_Lock();
    }
    
    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_EraseHeader( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_EraseHeader" );

    BaseType_t xReturn = pdTRUE;

    BOOT_LOG_L3( "[%s] This function is kept for compatibility with the generic bootloader.\r\n",
                         BOOT_METHOD_NAME );
    
    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_EraseBank( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_EraseBank" );
    
    BaseType_t xReturn = pdFALSE;
    uint8_t ucFlashArea;

    ucFlashArea = BOOT_FLASH_GetFlashArea( pxAppDescriptor );

    switch( ucFlashArea )
    {
		case( FLASH_PARTITION_IMAGE_0 ):
		{
#if 0
			BOOT_LOG_L3( "[%s] Restoring backup image to : 0x%08x\r\n", BOOT_METHOD_NAME, pxAppDescriptor );

			xReturn = AWS_RollBackFlashImage();

			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );
			}
			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
			}
#endif

			xReturn = pdTRUE;

			break;
		}
		case( FLASH_PARTITION_IMAGE_1 ):
		{
			xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );

			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
			}

			if(xReturn == pdTRUE)
			{
				BOOT_LOG_L2( "[%s] Bank erased at : 0x%08x\r\n",BOOT_METHOD_NAME, pxAppDescriptor);
			}
			else
			{
				BOOT_LOG_L2( "[%s] Bank erase failed at : 0x%08x\r\n",BOOT_METHOD_NAME, pxAppDescriptor);
			}
			break;
		}
		default:
		{
			break;
		}
    }

    return xReturn;
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_ValidateAddress( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_ValidateAddress" );

    BaseType_t xReturn = pdFALSE;

    /* Application start address needs to be >= than this limit. */
    const void * const pvStartAddressLimit = ( const void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_0 + sizeof( BOOTImageHeader_t ) );

    /* End address needs to be < than this limit.*/
    const void * const pvEndAddressLimit = ( const void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_0 + FLASH_IMAGE_SIZE_MAX );

    /* Validate the start address.*/
    xReturn = ( ( pxAppDescriptor->pvStartAddress >= pvStartAddressLimit ) &&
                ( pxAppDescriptor->pvStartAddress < pvEndAddressLimit ) );

    BOOT_LOG_L3( "[%s] Start Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvStartAddress );

    /* Validate the end address.*/
    xReturn = xReturn && ( ( pxAppDescriptor->pvEndAddress > pvStartAddressLimit ) &&
                           ( pxAppDescriptor->pvEndAddress <= pvEndAddressLimit ) );

    BOOT_LOG_L3( "[%s] End Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvEndAddress );


    /* Validate the execution address.*/
    xReturn = xReturn && ( ( pxAppDescriptor->pvExecAddress >= pxAppDescriptor->pvStartAddress ) &&
                           ( pxAppDescriptor->pvExecAddress < pxAppDescriptor->pvEndAddress ) );

    BOOT_LOG_L3( "[%s] Exe Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvExecAddress );
    
     /* Validate the start address is less than end address.*/
    xReturn = xReturn &&  ( pxAppDescriptor->pvStartAddress < pxAppDescriptor->pvEndAddress );

    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t AWS_FlashErasePartition( uint8_t ucFlashArea )
{
  uint32_t ulError = 0UL;
  
  /* Partition Image 0 always occupies the same region in FLASH_BANK1. */
  if ( FLASH_PARTITION_IMAGE_0 == ucFlashArea )
  {
    FLASH_EraseInitTypeDef xEraseInit =
    {
      .TypeErase      = FLASH_TYPEERASE_PAGES,
      .Banks          = FLASH_BANK_1,
      .Page           = 2 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR,
      .NbPages        = 6 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
      return pdFAIL;
    }
    if (0xFFFFFFFFUL != ulError )
    {
      return pdFAIL;
    }
  }
  /* Since the STM32L475 performs a sector by sector swap of OTA firmware images,
     Partition Image 1 only refers to a new OTA image and not a rollback image.
     As such, only the first sector of the image (with the header) must be
     erased. */
  else if ( FLASH_PARTITION_IMAGE_1 == ucFlashArea )
  {
    FLASH_EraseInitTypeDef xEraseInit =
    {
      .TypeErase      = FLASH_TYPEERASE_PAGES,
      .Banks          = FLASH_BANK_2,
      .Page           = 1 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR,
      .NbPages        = 1 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
      return pdFAIL;
    }
    if (0xFFFFFFFFUL != ulError )
    {
      return pdFAIL;
    }
  }
  /* Since the STM32L475 performs a sector by sector swap of OTA firmware images,
     Partition Image 2 only refers to a rollback image and not a new OTA image.
     This partition should only be erased if there is no valid Partition Image 1. */
  else if ( FLASH_PARTITION_IMAGE_BACKUP == ucFlashArea )
  {
    BOOTImageDescriptor_t * pxPartitionImage1 = ( void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_1 );
    
    if( memcmp( pxPartitionImage1->xImageHeader.acMagicCode,
                BOOT_MAGIC_CODE,
                BOOT_MAGIC_CODE_SIZE ) != 0 )
    {
        FLASH_EraseInitTypeDef xEraseInit =
        {
            .TypeErase      = FLASH_TYPEERASE_PAGES,
            .Banks          = FLASH_BANK_2,
            .Page           = 2 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR,
            .NbPages        = 5 * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR
        };
        
        HAL_FLASH_Unlock();
        
        if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
        {
          return pdFAIL;
        }
        if (0xFFFFFFFFUL != ulError )
        {
          return pdFAIL;
        }
    }
  }
  else
  {
    return pdFAIL;
  }
  
  HAL_FLASH_Lock();
  
  return pdPASS;
}

/*-----------------------------------------------------------*/

uint32_t FLASH_GetBank( uint32_t ulAddress )
{
	uint32_t ulFlashBank = FLASH_BANK_BOTH;

	if( ( ulAddress >= FLASH_BASE ) && ( ulAddress < ( FLASH_BASE + FLASH_BANK_SIZE ) ) )
	{
		ulFlashBank = FLASH_BANK_1;
	}
	else if( ( ulAddress >= ( FLASH_BASE + FLASH_BANK_SIZE ) ) && ( ulAddress < ( FLASH_BASE + FLASH_SIZE ) ) )
	{
		ulFlashBank = FLASH_BANK_2;
	}

	return ulFlashBank;
}

uint32_t FLASH_GetSector( uint32_t ulAddress )
{
	uint32_t ulFlashSector = 0xFFFFFFFFUL;

	if( ( ulAddress >= FLASH_BASE ) && ( ulAddress < ( FLASH_BASE + FLASH_BANK_SIZE ) ) )
	{
		ulFlashSector = ( ulAddress - FLASH_BASE ) / FLASH_PARTITION_IMAGE_SECTOR_SIZE;
	}
	else if( ( ulAddress >= ( FLASH_BASE + FLASH_BANK_SIZE ) ) && ( ulAddress < ( FLASH_BASE + FLASH_SIZE ) ) )
	{
		ulFlashSector = ( ulAddress - FLASH_BASE - FLASH_BANK_SIZE ) / FLASH_PARTITION_IMAGE_SECTOR_SIZE;;
	}

	return ulFlashSector;
}

uint32_t FLASH_GetPage( uint32_t ulAddress )
{
	uint32_t ulFlashPage = 0xFFFFFFFFUL;

	if( ( ulAddress >= FLASH_BASE ) && ( ulAddress < ( FLASH_BASE + FLASH_BANK_SIZE ) ) )
	{
		ulFlashPage = ( ulAddress - FLASH_BASE ) / FLASH_PAGE_SIZE;
	}
	else if( ( ulAddress >= ( FLASH_BASE + FLASH_BANK_SIZE ) ) && ( ulAddress < ( FLASH_BASE + FLASH_SIZE ) ) )
	{
		ulFlashPage = ( ulAddress - FLASH_BASE - FLASH_BANK_SIZE ) / FLASH_PAGE_SIZE;;
	}

	return ulFlashPage;
}

#define DESTINATION_ADDRESS ( 0U )
#define SOURCE_ADDRESS 		( 1U )

static uint32_t const SwapStep[12][2] =
{
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_5 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_5 ) },
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_5 ),		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_5 )	},
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_4 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_4 ) },
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_4 ), 		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_4 )	},
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_3 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_3 )	},
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_3 ), 		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_3 )	},
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_2 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_2 )	},
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_2 ), 		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_2 )	},
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_1 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_1 )	},
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_1 ), 		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_1 )	},
    { BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 ), 	MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 )	},
    { MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 ), 		OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 )	}
};

BaseType_t AWS_FlashEraseSector( uint32_t ulBank, uint32_t ulSector )
{
    BaseType_t xReturn = pdPASS;
    
    uint32_t ulError = 0UL;
    
    FLASH_EraseInitTypeDef xEraseInit =
    {
        .TypeErase    = FLASH_TYPEERASE_PAGES,
        .Banks        = ulBank,
        .Page         = ulSector * FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR,
        .NbPages      = FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
        xReturn = pdFAIL;
    }
    
    if (0xFFFFFFFFUL != ulError )
    {
        xReturn = pdFAIL;
    }
    
    HAL_FLASH_Lock();
    
    return xReturn;
}

BaseType_t AWS_FlashSwapSetFlag( BaseType_t Flag )
{
	BaseType_t xReturn = pdFAIL;

	uint32_t * Address = NULL;
	uint32_t Zeroes = 0UL;

	switch( Flag )
	{
		case( SWAP_STARTED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_STARTED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_5_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_5_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_4_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_4_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_3_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_3_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_2_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_2_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_1_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_1_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_0_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_0_ERASED_FLAG_ADDRESS );
			break;
		}
		case( SWAP_COMPLETED ):
		{
			Address = (uint32_t *) ( MAIN_IMAGE_BASE + SWAP_COMPLETED_FLAG_ADDRESS );
			break;
		}
		default:
		{
			return pdFAIL;
		}
	}

	xReturn = BOOT_FLASH_Write( Address, &Zeroes, 32 );

	return xReturn;
}

BaseType_t AWS_FlashSwapSectors( uint32_t Step )
{
    BaseType_t xReturn = pdFAIL;
    
    /* Clear the destination sector. */
    uint32_t const ulBank = FLASH_GetBank( SwapStep[Step][DESTINATION_ADDRESS] );
    uint32_t const ulSector = FLASH_GetSector( SwapStep[Step][DESTINATION_ADDRESS] );

    xReturn = AWS_FlashEraseSector( ulBank, ulSector );
    
    if ( pdPASS == xReturn )
    {
        /* Write the destination sector with data from the source sector. */
        uint32_t * pulAddress = (uint32_t *) SwapStep[Step][DESTINATION_ADDRESS];
        uint32_t * pulData    = (uint32_t *) SwapStep[Step][SOURCE_ADDRESS];
        int32_t lLength       = FLASH_PARTITION_IMAGE_SECTOR_SIZE;
        
        if( (uint32_t) pulAddress == MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 ) )
        {
            xReturn = BOOT_FLASH_Write( pulAddress,
                                        pulData,
                                        64 );

            if( pdPASS == xReturn )
            {
                pulAddress = (uint32_t *) ( SwapStep[Step][DESTINATION_ADDRESS] + 0x00A0 );
                pulData = (uint32_t *) ( SwapStep[Step][SOURCE_ADDRESS] + 0x00A0 );

                xReturn = BOOT_FLASH_Write( pulAddress,
                                            pulData,
                                            224 );

                if( pdPASS == xReturn )
                {
                    pulAddress = (uint32_t *) ( SwapStep[Step][DESTINATION_ADDRESS] + 0x01A0 );
                    pulData = (uint32_t *) ( SwapStep[Step][SOURCE_ADDRESS] + 0x01A0 );

                    xReturn = BOOT_FLASH_Write( pulAddress,
                                                pulData,
                                                lLength - 0x01A0 );
                }
            }
        }
        else
        {
            xReturn = BOOT_FLASH_Write( pulAddress,
                                        pulData,
                                        lLength );
        }
    }
    
    return pdPASS;
}

BaseType_t AWS_CopyOTAImageToMainApplication( void )
{
    BaseType_t xReturn = pdPASS;
    uint8_t ucStep = 0U;
    
    do
    {
        if (0 == ucStep)
        {
        	AWS_FlashSwapSetFlag( SWAP_STARTED );
        }
        if (1 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_5_ERASED );
        }
        if (3 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_4_ERASED );
        }
        if (5 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_3_ERASED );
        }
        if (7 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_2_ERASED );
        }
        if (9 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_1_ERASED );
        }
        if (11 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_0_ERASED );
        }
        
        xReturn = AWS_FlashSwapSectors( ucStep );
    } while ( ( ++ucStep < 12 ) && ( pdPASS == xReturn ) );
    
    /* Erase the first OTA image sector when complete. */
    if ( ( pdPASS == xReturn ) && 
         ( pdFAIL == AWS_FlashEraseSector( FLASH_BANK_2, OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 ) ) ) )
    {
        xReturn = pdFAIL;
    }
    else
    {
    	AWS_FlashSwapSetFlag( SWAP_COMPLETED );
    }
    
    return xReturn;
}

BaseType_t prvRollBackSector( uint8_t ucSector )
{
    BaseType_t xReturn = pdFAIL;
    
    /* Clear the destination sector. */
    uint32_t const ulBank = FLASH_GetBank( MAIN_IMAGE_SECTOR_ADDRESS( ucSector ) );
    uint32_t const ulSector = FLASH_GetSector( MAIN_IMAGE_SECTOR_ADDRESS( ucSector ) );

    xReturn = AWS_FlashEraseSector( ulBank, ulSector );
    
    if ( pdPASS == xReturn )
    {
      uint32_t * pulAddress = (uint32_t *) ( MAIN_IMAGE_SECTOR_ADDRESS( ucSector ) );
      uint32_t * pulData    = (uint32_t *) ( BACKUP_IMAGE_SECTOR_ADDRESS( ucSector ) );
      int32_t const lLength = FLASH_PARTITION_IMAGE_SECTOR_SIZE;
      
      xReturn = BOOT_FLASH_Write( pulAddress,
                                 pulData,
                                 lLength );
    }
    
    return xReturn;
}

BaseType_t AWS_RollBackFlashImage( void )
{
    BaseType_t xReturn = pdPASS;
    
    BOOTImageHeader_t * pxImageHeader = (BOOTImageHeader_t *) OTA_IMAGE_BASE;
    
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector0Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_0 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector1Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_1 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector2Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_2 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector3Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_3 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector4Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_4 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulSector5Erased == 0x00000000UL ) )
    {
        xReturn = prvRollBackSector( IMAGE_SECTOR_5 );
    }
    if ( ( pdFAIL != xReturn ) && ( pxImageHeader->ulImageSwapStarted == 0x00000000UL ) )
    {
        xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );
        
        if ( pdPASS == xReturn )
        {
            xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
        }
    }
    
    return xReturn;
}
