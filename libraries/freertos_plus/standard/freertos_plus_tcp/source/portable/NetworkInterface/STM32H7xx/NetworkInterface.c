/**
  ******************************************************************************
  * File Name          : NetworkInterace.h
  * Description        : This file provides code that defines interfaces the
  *	                     ethernet driver to the FreeRTOS+TCP portable layer.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP_Private.h"
#include "FreeRTOS_DNS.h"
#include "NetworkBufferManagement.h"
#include "NetworkInterface.h"

/* ST includes. */
#include "stm32h7xx_hal.h"
#include "lan8742.h"

/* Interrupt events to process.  Currently only the Tx and Rx events are
 * processed although code for other events is included to allow for possible
 * future expansion.
 */
#define EMAC_IF_RX_EVENT        1UL
#define EMAC_IF_TX_EVENT        2UL
#define EMAC_IF_ERR_EVENT       4UL
#define EMAC_IF_ALL_EVENT       ( EMAC_IF_RX_EVENT | EMAC_IF_TX_EVENT | EMAC_IF_ERR_EVENT )

/* Default the size of the stack used by the EMAC deferred handler task to twice
 * the size of the stack used by the idle task - but allow this to be overridden
 * in FreeRTOSConfig.h as configMINIMAL_STACK_SIZE is a user definable constant.
 */
#ifndef configEMAC_TASK_STACK_SIZE
#define configEMAC_TASK_STACK_SIZE ( 2 * configMINIMAL_STACK_SIZE )
#endif

/* A deferred interrupt handler task that processes. */
static void prvEMACHandlerTask( void *pvParameters );

/* See if there is a new packet and forward it to the IP-task. */
static BaseType_t prvNetworkInterfaceInput( void );

/* Check if a given packet should be accepted. */
static BaseType_t xMayAcceptPacket( uint8_t *pcBuffer );

/*-----------------------------------------------------------*/

/*
 * Functions related to accessing the Ethernet Phy.
 */
int32_t ETH_PHY_IO_Init( void );
int32_t ETH_PHY_IO_DeInit ( void );
int32_t ETH_PHY_IO_ReadReg( uint32_t DevAddr,
                            uint32_t RegAddr,
                            uint32_t *pRegVal);
int32_t ETH_PHY_IO_WriteReg( uint32_t DevAddr,
                             uint32_t RegAddr,
                             uint32_t RegVal);
int32_t ETH_PHY_IO_GetTick( void );

lan8742_Object_t LAN8742;

lan8742_IOCtx_t  LAN8742_IOCtx =
{
  ETH_PHY_IO_Init,
  ETH_PHY_IO_DeInit,
  ETH_PHY_IO_WriteReg,
  ETH_PHY_IO_ReadReg,
  ETH_PHY_IO_GetTick
};

/*-----------------------------------------------------------*/

/* Bit map of outstanding ETH interrupt events for processing.  Currently only
 * the Rx interrupt is handled, although code is included for other events to
 * enable future expansion.
 */
volatile uint32_t ulISREvents;

#if( ipconfigUSE_LLMNR == 1 )
static const uint8_t xLLMNR_MACAddress[] = { 0x01, 0x00, 0x5E, 0x00, 0x00, 0xFC };
#endif

/* Ethernet handle. */
static ETH_HandleTypeDef heth;

/* xTXDescriptorSemaphore is a counting semaphore with a maximum count of
 * ETH_RX_DESC_CNT, which is the number of DMA TX descriptors.
 */
static SemaphoreHandle_t xTXDescriptorSemaphore = NULL;

ETH_TxPacketConfig TxConfig;

#if defined ( __ICCARM__ ) /*!< IAR Compiler */

/* Ethernet Rx DMA Descriptors */
#pragma data_alignment = 4
#pragma location=0x30040000
ETH_DMADescTypeDef DMARxDscrTab[ ETH_RX_DESC_CNT ];

/* Ethernet Tx DMA Descriptors */
#pragma data_alignment = 4
#pragma location=0x30040060
ETH_DMADescTypeDef DMATxDscrTab[ ETH_TX_DESC_CNT ];

/* Ethernet Receive Buffers */
#pragma data_alignment = 4
#pragma location=0x30040200
uint8_t Rx_Buff[ ETH_RX_DESC_CNT ][ ETH_MAX_PACKET_SIZE ];

#elif defined ( __CC_ARM )  /* MDK ARM Compiler */

/* Ethernet Rx DMA Descriptors */
__attribute__ ( ( at( 0x30040000 ) ) ) ETH_DMADescTypeDef DMARxDscrTab[ ETH_RX_DESC_CNT ];

/* Ethernet Tx DMA Descriptors */
__attribute__ ( ( at( 0x30040060 ) ) ) ETH_DMADescTypeDef DMATxDscrTab[ ETH_TX_DESC_CNT ];

/* Ethernet Receive Buffer */
__attribute__ ( ( at( 0x30040200 ) ) ) uint8_t Rx_Buff[ ETH_RX_DESC_CNT ][ ETH_MAX_PACKET_SIZE ];

#elif defined ( __GNUC__ ) /* GNU Compiler */

/* Ethernet Rx DMA Descriptors */
__attribute__ ( ( aligned ( 32 ) ) ) ETH_DMADescTypeDef DMARxDscrTab[ ETH_RX_DESC_CNT ] __attribute__ ( ( section( ".RxDecripSection" ) ) );

/* Ethernet Tx DMA Descriptors */
__attribute__ ( ( aligned ( 32 ) ) ) ETH_DMADescTypeDef DMATxDscrTab[ ETH_TX_DESC_CNT ] __attribute__ ( ( section( ".TxDecripSection" ) ) );

/* Ethernet Receive Buffers */
__attribute__ ( ( aligned ( 32 ) ) ) uint8_t Rx_Buff[ ETH_RX_DESC_CNT ][ ETH_MAX_PACKET_SIZE ] __attribute__ ( ( section( ".RxArraySection" ) ) );

#endif

/* ucMACAddress as it appears in main.c */
extern const uint8_t ucMACAddress[ 6 ];

/* Holds the handle of the task used as a deferred interrupt processor.  The
 * handle is used so direct notifications can be sent to the task for all
 * EMAC/DMA related interrupts.
 */
static TaskHandle_t xEMACTaskHandle = NULL;

/*-----------------------------------------------------------*/

void HAL_ETH_RxCpltCallback( ETH_HandleTypeDef *heth )
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* Ethernet RX-Complete callback function, elsewhere declared as weak. */
    ulISREvents |= EMAC_IF_RX_EVENT;

    /* Wakeup the prvEMACHandlerTask. */
    if( xEMACTaskHandle != NULL )
    {
        vTaskNotifyGiveFromISR( xEMACTaskHandle, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }
}
/*-----------------------------------------------------------*/
void HAL_ETH_TxCpltCallback( ETH_HandleTypeDef *heth )
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* This call-back is only useful in case packets are being sent
     * zero-copy.  Once they're sent, the buffers will be released
     * by the function vClearTXBuffers().
     */
    ulISREvents |= EMAC_IF_TX_EVENT;

    /* Wakeup the prvEMACHandlerTask. */
    if( xEMACTaskHandle != NULL )
    {
        vTaskNotifyGiveFromISR( xEMACTaskHandle, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }
}
/*-----------------------------------------------------------*/

/* DMATxDescToClear points to the next TX DMA descriptor that must be cleared
 * by vClearTXBuffers().
 */
static __IO ETH_DMADescTypeDef  *DMATxDescToClear;

static void vClearTXBuffers()
{
    __IO ETH_DMADescTypeDef * txLastDescriptor = ( ETH_DMADescTypeDef * ) heth.TxDescList.TxDesc[ heth.TxDescList.CurTxDesc ];
    size_t uxCount = ( ( UBaseType_t ) ETH_TX_DESC_CNT ) - uxSemaphoreGetCount( xTXDescriptorSemaphore );

#if( ipconfigZERO_COPY_TX_DRIVER != 0 )
    NetworkBufferDescriptor_t * pxNetworkBuffer;
    uint8_t * ucPayLoad;
#endif
    /* This function is called after a TX-completion interrupt.
     * It will release each Network Buffer used in xNetworkInterfaceOutput().
     * 'uxCount' represents the number of descriptors given to DMA for transmission.
     * After sending a packet, the DMA will clear the 'ETH_DMATXNDESCWBF_OWN' bit.
     */
    while( ( uxCount > 0 )  &&
           ( ( DMATxDescToClear->DESC3 & ETH_DMATXNDESCWBF_OWN ) == 0 ) )
    {
        if( ( DMATxDescToClear == txLastDescriptor ) &&
            ( uxCount != ETH_TX_DESC_CNT ) )
        {
            break;
        }
#if( ipconfigZERO_COPY_TX_DRIVER != 0 )
        {
            ucPayLoad = ( uint8_t * )DMATxDescToClear->DESC0;

            if( ucPayLoad != NULL )
            {
                pxNetworkBuffer = pxPacketBuffer_to_NetworkBuffer( ucPayLoad );
                
                if( pxNetworkBuffer != NULL )
                {
                    vReleaseNetworkBufferAndDescriptor( pxNetworkBuffer );
                }
            }
        }
#endif /* ipconfigZERO_COPY_TX_DRIVER */

        if ( DMATxDescToClear == ( ETH_DMADescTypeDef * ) heth.TxDescList.TxDesc[ ( ETH_TX_DESC_CNT - 1 ) ] )
        {
            DMATxDescToClear = DMATxDscrTab;
        }
        else
        {
            DMATxDescToClear++;
        }

        uxCount--;

        /* Tell the counting semaphore that one more TX descriptor is available. */
        xSemaphoreGive( xTXDescriptorSemaphore );
    }
}

BaseType_t xNetworkInterfaceInitialise( void )
{
    BaseType_t xResult = pdFAIL;;

    uint32_t idx = 0;
    HAL_StatusTypeDef hal_eth_init_status = HAL_ERROR;

    if( xEMACTaskHandle == NULL )
    {
        if( xTXDescriptorSemaphore == NULL )
        {
            xTXDescriptorSemaphore = xSemaphoreCreateCounting( ( UBaseType_t ) ETH_TX_DESC_CNT, ( UBaseType_t ) ETH_TX_DESC_CNT );
            configASSERT( xTXDescriptorSemaphore );
        }

        /* Initialise ETH */
        heth.Instance = ETH;
        heth.Init.MACAddr = ( uint8_t * ) ucMACAddress;
        heth.Init.MediaInterface = HAL_ETH_RMII_MODE;
        heth.Init.TxDesc = DMATxDscrTab;
        heth.Init.RxDesc = DMARxDscrTab;
        heth.Init.RxBuffLen = ETH_MAX_PACKET_SIZE;

        hal_eth_init_status = HAL_ETH_Init( &heth );

        for( idx = 0; idx < ETH_RX_DESC_CNT; idx++ )
        {
            HAL_ETH_DescAssignMemory( &heth, idx, Rx_Buff[idx], NULL );
        }

        /* Initialize Tx Descriptors list: Chain Mode */
        DMATxDescToClear = DMATxDscrTab;

        memset( &TxConfig, 0 , sizeof( ETH_TxPacketConfig ) );

        TxConfig.Attributes = ETH_TX_PACKETS_FEATURES_CSUM |
							  ETH_TX_PACKETS_FEATURES_CRCPAD;
        TxConfig.ChecksumCtrl = ETH_CHECKSUM_IPHDR_PAYLOAD_INSERT_PHDR_CALC;
        TxConfig.CRCPadCtrl = ETH_CRC_PAD_INSERT;

        /* Only for inspection by debugger. */
        ( void ) hal_eth_init_status;

        /* The deferred interrupt handler task is created at the highest
         * possible priority to ensure the interrupt handler can return directly
         * to it. The task's handle is stored in xEMACTaskHandle so interrupts
         * can notify the task when there is something to process.
         */
        xTaskCreate( prvEMACHandlerTask,
					 "EMAC",
					 configEMAC_TASK_STACK_SIZE,
					 NULL,
					 configMAX_PRIORITIES - 1,
					 &xEMACTaskHandle );
    } /* if( xEMACTaskHandle == NULL ) */

    /* Set PHY IO functions */
    LAN8742_RegisterBusIO( &LAN8742, &LAN8742_IOCtx );

    /* Initialize the LAN8742 ETH PHY */
    LAN8742_Init( &LAN8742 );

    if ( hal_eth_init_status == HAL_OK )
    {
        uint32_t duplex, speed = 0;
        int32_t PHYLinkState;
        ETH_MACConfigTypeDef MACConf;
        PHYLinkState = LAN8742_GetLinkState( &LAN8742 );

        /* Get link state */
        if( PHYLinkState <= LAN8742_STATUS_LINK_DOWN )
        {
            /* For now pdFAIL will be returned. But prvEMACHandlerTask() is
             * running and it will keep on checking the PHY and set
             * 'ulLinkStatusMask' when necessary.
			 */
            xResult = pdFAIL;
            FreeRTOS_printf( ( "Link Status still low\n" ) ) ;
        }
        else
        {
            xResult = pdPASS;

            switch ( PHYLinkState )
            {
                case LAN8742_STATUS_100MBITS_FULLDUPLEX:
				{
                    duplex = ETH_FULLDUPLEX_MODE;
                    speed = ETH_SPEED_100M;
                    break;
				}
                case LAN8742_STATUS_100MBITS_HALFDUPLEX:
				{
                    duplex = ETH_HALFDUPLEX_MODE;
                    speed = ETH_SPEED_100M;
                    break;
				}
                case LAN8742_STATUS_10MBITS_FULLDUPLEX:
				{
                    duplex = ETH_FULLDUPLEX_MODE;
                    speed = ETH_SPEED_10M;
                    break;
				}
                case LAN8742_STATUS_10MBITS_HALFDUPLEX:
				{
                    duplex = ETH_HALFDUPLEX_MODE;
                    speed = ETH_SPEED_10M;
                    break;
				}
                default:
				{
                    duplex = ETH_FULLDUPLEX_MODE;
                    speed = ETH_SPEED_100M;
                    break;
				}
            }

            /* Get MAC Config MAC */
            FreeRTOS_printf( ( "Link Status high\n" ) ) ;
            HAL_ETH_GetMACConfig( &heth, &MACConf );
            MACConf.DuplexMode = duplex;
            MACConf.Speed = speed;
            HAL_ETH_SetMACConfig( &heth, &MACConf );

            HAL_ETH_Start_IT( &heth );
        }
    }

    /* When returning non-zero, the stack will become active and start DHCP (in
     * configured)
     */
    return xResult;
}

/*-----------------------------------------------------------*/
BaseType_t xNetworkInterfaceOutput( NetworkBufferDescriptor_t * const pxDescriptor,
									BaseType_t bReleaseAfterSend )
{
    BaseType_t xReturn = pdFAIL;
    uint32_t ulTransmitSize = 0;
	ETH_BufferTypeDef Txbuffer;
	
    /* Do not wait too long for a free TX DMA buffer. */
    const TickType_t xBlockTimeTicks = pdMS_TO_TICKS( 50u );

    memset( ( void * ) &Txbuffer, 0 , sizeof( ETH_BufferTypeDef ) );

#if( ipconfigDRIVER_INCLUDED_TX_IP_CHECKSUM != 0 )
    {
        ProtocolPacket_t *pxPacket;

#if( ipconfigZERO_COPY_TX_DRIVER != 0 )
        {
            /* Confirm that the pxDescriptor may be kept by the driver. */
            configASSERT( bReleaseAfterSend != 0 );
        }
#endif /* ipconfigZERO_COPY_RX_DRIVER */

        /*
         * If the peripheral must calculate the checksum, it wants
         * the protocol checksum to have a value of zero. For ICMP-over-IPv4 packets,
         * the Checksum field in the ICMP packet must always be 0x0000 in both modes,
         * because pseudo-headers are not defined for such packets. (RM0433 Rev 5,
         * p 2786, Note)
         */
        pxPacket = ( ProtocolPacket_t * ) ( pxDescriptor->pucEthernetBuffer );

        if( pxPacket->xICMPPacket.xIPHeader.ucProtocol == ipPROTOCOL_ICMP )
        {
            pxPacket->xICMPPacket.xICMPHeader.usChecksum = ( uint16_t ) 0u;
        }
    }
#endif

    /* Open a do {} while ( 0 ) loop to be able to call break. */
    do
    {
        if ( pdFAIL == xGetPhyLinkStatus() )
        {
            /* Do not attempt to send packets as long as the Link Status is low.
  			 */
            break;
        }

        if( xSemaphoreTake( xTXDescriptorSemaphore, xBlockTimeTicks ) != pdPASS )
        {
            /* Time-out waiting for a free TX descriptor. */
            break;
        }

        /* Get bytes in current buffer. */
        ulTransmitSize = pxDescriptor->xDataLength;

        if( ulTransmitSize > ETH_MAX_PACKET_SIZE )
        {
            ulTransmitSize = ETH_MAX_PACKET_SIZE;
        }

        /* Copy the bytes. */
        Txbuffer.buffer = pxDescriptor->pucEthernetBuffer;

        Txbuffer.len = ulTransmitSize;
        Txbuffer.next = NULL;

        TxConfig.Length = Txbuffer.len;
        TxConfig.TxBuffer = &Txbuffer;

        /* Clean and Invalidate data cache */
        SCB_CleanInvalidateDCache();

        if( HAL_OK == HAL_ETH_Transmit_IT( &heth, &TxConfig ) )
        {
            iptraceNETWORK_INTERFACE_TRANSMIT();

            bReleaseAfterSend = pdFALSE;

            xReturn = pdPASS;
        }
    } while( 0 );

    /* The buffer has been sent so can be released. */
    if( bReleaseAfterSend != pdFALSE )
    {
        vReleaseNetworkBufferAndDescriptor( pxDescriptor );
    }

    return xReturn;
}
/*-----------------------------------------------------------*/

#if defined ( __ICCARM__ ) /*!< IAR Compiler */

#pragma data_alignment=32
#pragma location=0x24000000
static uint8_t ucNetworkPackets[ ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS * ETH_MAX_PACKET_SIZE ];

#elif defined ( __GNUC__ ) /* GNU Compiler */

__attribute__ ( ( aligned ( 32 ) ) ) uint8_t ucNetworkPackets[ ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS * ETH_MAX_PACKET_SIZE ] __attribute__ ( ( section( ".pNetBuff" ) ) );

#endif

void vNetworkInterfaceAllocateRAMToBuffers( NetworkBufferDescriptor_t pxNetworkBuffers[ ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS ] )
{
    uint8_t *ucRAMBuffer = ucNetworkPackets;
    uint32_t ul;

    for( ul = 0; ul < ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS; ul++ )
    {
        pxNetworkBuffers[ ul ].pucEthernetBuffer = ucRAMBuffer + ipBUFFER_PADDING;
        *( ( unsigned * ) ucRAMBuffer ) = ( unsigned ) ( &( pxNetworkBuffers[ ul ] ) );
        ucRAMBuffer += ETH_MAX_PACKET_SIZE;
    }
}

/*-----------------------------------------------------------*/
static BaseType_t xMayAcceptPacket( uint8_t * pcBuffer )
{
    const ProtocolPacket_t * pxProtPacket = ( const ProtocolPacket_t * ) pcBuffer;

    switch( pxProtPacket->xTCPPacket.xEthernetHeader.usFrameType )
    {
        case ipARP_FRAME_TYPE:
		{
            /* Check it later. */
            return pdTRUE;
		}
        case ipIPv4_FRAME_TYPE:
		{
            /* Check it here. */
            break;
		}
        default:
		{
            /* Refuse the packet. */
            return pdFALSE;
		}
    }

    return pdTRUE;
}
/*-----------------------------------------------------------*/

static BaseType_t prvNetworkInterfaceInput( void )
{
    ETH_BufferTypeDef RxBuffer = { 0 };
    NetworkBufferDescriptor_t *pxDescriptor;
    BaseType_t xReceivedLength = 0, xAccepted = pdFALSE;
    uint32_t bufferLength = 0;
    xIPStackEvent_t xRxEvent = { eNetworkRxEvent, 0 };
    const TickType_t xDescriptorWaitTime = pdMS_TO_TICKS( 500 );
    uint8_t *pucBuffer;

    /* Clean and Invalidate data cache */
    SCB_CleanInvalidateDCache();

    if ( HAL_ETH_GetRxDataBuffer(&heth, &RxBuffer) == HAL_OK )
    {
        HAL_ETH_GetRxDataLength(&heth, &bufferLength);

        xReceivedLength = bufferLength;
        pucBuffer = RxBuffer.buffer;

        /* See if this packet must be handled. */
        xAccepted = xMayAcceptPacket( pucBuffer );
    }

    if ( xAccepted != pdFALSE )
    {
        /* The packet wil be accepted, but check first if a new Network Buffer
		 * can be obtained. If not, the packet will still be dropped.
		 */
        pxDescriptor = pxGetNetworkBufferWithDescriptor( xReceivedLength, xDescriptorWaitTime );

        if( pxDescriptor == NULL )
        {
            /* A new descriptor can not be allocated now. This packet will be
 			 * dropped.
			 */
            xAccepted = pdFALSE;
        }
    }

    if ( xAccepted != pdFALSE )
    {
        memcpy( pxDescriptor->pucEthernetBuffer, pucBuffer, xReceivedLength );

        /* This example assumes that the DMADescriptor_t type has a member
         * called pucEthernetBuffer that points to the Ethernet buffer
         * containing the received data, and a member called xDataLength that
         * holds the length of the received data.  Update the newly allocated
         * network buffer descriptor to point to the Ethernet buffer that
		 * contains the received data.
         */
        pxDescriptor->xDataLength = xReceivedLength;

        /*
         * See if the data contained in the received Ethernet frame needs to be
         * processed.  NOTE! It might be possible to do this in the the
         * interrupt service routine itself, which would remove the need to
         * unblock this task for packets that don't need processing.
         */
        if( eConsiderFrameForProcessing( pxDescriptor->pucEthernetBuffer ) == eProcessBuffer )
        {
            /* The event about to be sent to the TCP/IP is an Rx event. */
            xRxEvent.eEventType = eNetworkRxEvent;

            /* pvData is used to point to the network buffer descriptor that
             * references the received data.
			 */
            xRxEvent.pvData = ( void * ) pxDescriptor;

            /* Send the data to the TCP/IP stack. */
            if( xSendEventStructToIPTask( &xRxEvent, xDescriptorWaitTime ) == pdFALSE )
            {
                /* The buffer could not be sent to the IP task so the buffer
                 * must be released.
				 */
                vReleaseNetworkBufferAndDescriptor( pxDescriptor );

                /* Make a call to the standard trace macro to log the
 				 * occurrence.
				 */
                iptraceETHERNET_RX_EVENT_LOST();
            }
            else
            {
                /* The message was successfully sent to the TCP/IP stack. Call
                 * the standard trace macro to log the occurrence.
                 */
                iptraceNETWORK_INTERFACE_RECEIVE();
            }
        }
        else
        {
            /* The Ethernet frame can be dropped, but the Ethernet buffer must
             * be released.
             */
            vReleaseNetworkBufferAndDescriptor( pxDescriptor );
        }
    }

    /* Build Rx descriptor to be ready for next data reception. */
    HAL_ETH_BuildRxDescriptors( &heth );

    return ( xReceivedLength > 0 );
}
/*-----------------------------------------------------------*/

BaseType_t xGetPhyLinkStatus( void )
{
    BaseType_t xReturn;
    int linkState = LAN8742_GetLinkState(&LAN8742);

    switch (linkState)
    {
        case ( LAN8742_STATUS_100MBITS_FULLDUPLEX ):
        case ( LAN8742_STATUS_100MBITS_HALFDUPLEX ):
        case ( LAN8742_STATUS_10MBITS_FULLDUPLEX ):
        case ( LAN8742_STATUS_10MBITS_HALFDUPLEX ):
        {
            xReturn = pdPASS;
            break;
        }
        default:
        {
            xReturn = pdFAIL;
            break;
        }
    }

    return xReturn;
}
/*-----------------------------------------------------------*/

static void prvEMACHandlerTask( void *pvParameters )
{
    UBaseType_t uxLastMinBufferCount = 0;
    UBaseType_t uxCurrentCount;
    BaseType_t xResult = 0;
    const TickType_t ulMaxBlockTime = pdMS_TO_TICKS( 100UL );

    /* Remove compiler warnings about unused parameters. */
    ( void ) pvParameters;

    for( ;; )
    {
        uxCurrentCount = uxGetMinimumFreeNetworkBuffers();

        if( uxLastMinBufferCount != uxCurrentCount )
        {
            /* The logging produced below may be helpful while tuning +TCP:
			 * see how many buffers are in use.
			 */
            uxLastMinBufferCount = uxCurrentCount;
            FreeRTOS_printf( ( "Network buffers: %lu lowest %lu\n",
                             uxGetNumberOfFreeNetworkBuffers(), uxCurrentCount ) );
        }

        if( xTXDescriptorSemaphore != NULL )
        {
            static UBaseType_t uxLowestSemCount = ( UBaseType_t ) ETH_TX_DESC_CNT - 1;

            uxCurrentCount = uxSemaphoreGetCount( xTXDescriptorSemaphore );
            if( uxLowestSemCount > uxCurrentCount )
            {
                uxLowestSemCount = uxCurrentCount;
                FreeRTOS_printf( ( "TX DMA buffers: lowest %lu\n", uxLowestSemCount ) );
            }
        }

        if( ( ulISREvents & EMAC_IF_ALL_EVENT ) == 0 )
        {
            /* No events to process now, wait for the next. */
            ulTaskNotifyTake( pdFALSE, ulMaxBlockTime );
        }

        if( ( ulISREvents & EMAC_IF_RX_EVENT ) != 0 )
        {
            ulISREvents &= ~EMAC_IF_RX_EVENT;

            xResult = prvNetworkInterfaceInput();

            if( xResult > 0 )
            {
                while( prvNetworkInterfaceInput() > 0 )
                {
                }
            }
        }
        if( ( ulISREvents & EMAC_IF_TX_EVENT ) != 0 )
        {
            /* Code to release TX buffers if zero-copy is used. */
            ulISREvents &= ~EMAC_IF_TX_EVENT;

            /* Check if DMA packets have been delivered. */
            vClearTXBuffers();
        }
        if( ( ulISREvents & EMAC_IF_ERR_EVENT ) != 0 )
        {
            /* Future extension: logging about errors that occurred. */
            ulISREvents &= ~EMAC_IF_ERR_EVENT;
        }
        if( xResult < 0 )
        {
            uint32_t duplex, speed = 0;
            int32_t PHYLinkState;
            ETH_MACConfigTypeDef MACConf;
            PHYLinkState = LAN8742_GetLinkState(&LAN8742);

            /* Get link state */
            if(PHYLinkState <= LAN8742_STATUS_LINK_DOWN)
            {
                /* For now pdFAIL will be returned. But prvEMACHandlerTask() is
 				 * running and it will keep on checking the PHY and set
 				 * 'ulLinkStatusMask' when necessary.
				 */
                xResult = pdFAIL;
                HAL_ETH_Stop(&heth);
                FreeRTOS_printf( ( "Link Status still low\n" ) ) ;
            }
            else
            {
                xResult = pdPASS;

                switch (PHYLinkState)
                {
                    case LAN8742_STATUS_100MBITS_FULLDUPLEX:
					{
                        duplex = ETH_FULLDUPLEX_MODE;
                        speed = ETH_SPEED_100M;
                        break;
					}
                    case LAN8742_STATUS_100MBITS_HALFDUPLEX:
					{
                        duplex = ETH_HALFDUPLEX_MODE;
                        speed = ETH_SPEED_100M;
                        break;
					}
                    case LAN8742_STATUS_10MBITS_FULLDUPLEX:
					{
                        duplex = ETH_FULLDUPLEX_MODE;
                        speed = ETH_SPEED_10M;
                        break;
					}
                    case LAN8742_STATUS_10MBITS_HALFDUPLEX:
					{
                        duplex = ETH_HALFDUPLEX_MODE;
                        speed = ETH_SPEED_10M;
                        break;
					}
                    default:
					{
                        duplex = ETH_FULLDUPLEX_MODE;
                        speed = ETH_SPEED_100M;
                        break;
					}
                }

                /* Get MAC Config MAC */
                HAL_ETH_GetMACConfig(&heth, &MACConf);
                MACConf.DuplexMode = duplex;
                MACConf.Speed = speed;
                HAL_ETH_SetMACConfig(&heth, &MACConf);
                HAL_ETH_Start_IT(&heth);
                FreeRTOS_printf( ( "Link Status high\n" ) ) ;
            }
        }
    }
}
/*-----------------------------------------------------------*/
void ETH_IRQHandler( void )
{
    HAL_ETH_IRQHandler( &heth );
}

/*******************************************************************************
PHY IO Functions
*******************************************************************************/
/**
* @brief  Initializes the MDIO interface GPIO and clocks.
* @param  None
* @retval 0 if OK, -1 if ERROR
*/
int32_t ETH_PHY_IO_Init( void )
{
    /* We assume that MDIO GPIO configuration is already done in the
     * ETH_MspInit() else it should be done here
     */

    /* Configure the MDIO Clock */
    HAL_ETH_SetMDIOClockRange( &heth );

    return 0;
}

/**
* @brief  De-Initializes the MDIO interface .
* @param  None
* @retval 0 if OK, -1 if ERROR
*/
int32_t ETH_PHY_IO_DeInit( void )
{
    return 0;
}

/**
* @brief  Read a PHY register through the MDIO interface.
* @param  DevAddr: PHY port address
* @param  RegAddr: PHY register address
* @param  pRegVal: pointer to hold the register value
* @retval 0 if OK -1 if Error
*/
int32_t ETH_PHY_IO_ReadReg( uint32_t DevAddr, uint32_t RegAddr, uint32_t *pRegVal )
{
    if( HAL_ETH_ReadPHYRegister( &heth, DevAddr, RegAddr, pRegVal ) != HAL_OK )
    {
        return -1;
    }

    return 0;
}

/**
* @brief  Write a value to a PHY register through the MDIO interface.
* @param  DevAddr: PHY port address
* @param  RegAddr: PHY register address
* @param  RegVal: Value to be written
* @retval 0 if OK -1 if Error
*/
int32_t ETH_PHY_IO_WriteReg( uint32_t DevAddr, uint32_t RegAddr, uint32_t RegVal )
{
    if( HAL_ETH_WritePHYRegister( &heth, DevAddr, RegAddr, RegVal ) != HAL_OK )
    {
        return -1;
    }

    return 0;
}

/**
* @brief  Get the time in millisecons used for internal PHY driver process.
* @retval Time value
*/
int32_t ETH_PHY_IO_GetTick( void )
{
    return HAL_GetTick();
}
